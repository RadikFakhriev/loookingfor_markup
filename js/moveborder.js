$(window).load(function(){
	var ribbon = $('#news-block');
	ribbon.css({'position': 'relative'});
	ribbon.append('<div id="h-top-border"></div>').append('<div id="h-bottom-border"></div>').append('<div id="v-left-border"></div>').append('<div id="v-right-border"></div>');
	var activeItemBord = new ActiveBorder($('#h-top-border'), $('#h-bottom-border'), $('#v-left-border'), $('#v-right-border'));
	var newsItem = ribbon.find('li');

	activeItemBord.setWidthHBorders(ribbon.width());
	activeItemBord.setPosTop(0);
	activeItemBord.setPosLeft(0);
	activeItemBord.setHeightBorders(newsItem.height()+parseInt(newsItem.css('padding-top'))+parseInt(newsItem.css('padding-bottom')));



	newsItem.hover(
		function(){
			topPos = $(this).position().top;
			newHt = $(this).height()+parseInt($(this).css('padding-top'))+parseInt($(this).css('padding-bottom'));
			prevw = $(this).find('.news-preview');
			activeItemBord.animateBorderPos(topPos, newHt + parseInt(prevw.actual('height')));
			prevw.show();
		},
		function(){
			prevw.hide();
			activeItemBord.animateBorderPos(topPos, newHt);
			
		}
	)


	function ActiveBorder(topB,botB,leftB,rightB){
		this.topBorder = topB;
		this.bottomBorder = botB;
		this.leftBorder = leftB;
		this.rightBorder = rightB;

		this.currTopPos = null;

		this.getPosTopBorder = function(){
			return parseInt(this.topBorder.position.top);
		}

		this.getPosBottomBorder = function(){
			return parseInt(this.bottomBorder.position.top);
		}

		this.getVDepth = function(){
			return parseInt(this.topBorder.height() + this.bottomBorder.height());
		}

		this.getHWidth = function(){
			return parseInt(this.leftBorder.width() + this.rightBorder.width());
		}

		this.setPosTop = function(topIndent){
			this.topBorder.css({'top' : topIndent});
			this.leftBorder.css({'top' : topIndent});
			this.rightBorder.css({'top' : topIndent});
		}

	
		this.setPosLeft = function(leftIndent){
			this.leftBorder.css({'left' : leftIndent});
			this.rightBorder.css({'left' : parseInt(leftIndent) + this.topBorder.width() - this.leftBorder.width()});
		}


		this.setWidthHBorders = function(newWidth){
			this.topBorder.css({'width' : newWidth});
			this.bottomBorder.css({'width' : newWidth});
		}

		this.setHeightBorders = function(newHeight){
			this.leftBorder.css({'height' : newHeight});
			this.rightBorder.css({'height' : newHeight});
			this.bottomBorder.css({'top' : parseInt(newHeight)+this.topBorder.position().top});
		}

		this.animateBorderPos = function(targetTopPos, newHeight){
			this.currTopPos = targetTopPos;
			this.topBorder.stop().animate({'top' : targetTopPos}, 300);
			this.leftBorder.stop().animate({'top' : targetTopPos, 'height' : newHeight}, 300);
			this.rightBorder.stop().animate({'top' : targetTopPos, 'height' : newHeight}, 300);
			this.bottomBorder.stop().animate({'top' : parseInt(newHeight) + parseInt(this.currTopPos)}, 300);
		}

	}



});



