$(function(){

	if($('.chosen-select').length>0){
		$('.chosen-select').chosen({disable_search_threshold: 10});
	}
	

	$('.offer-name').hover(
		function(){
			$(this).parent().find('.offer-category').addClass('offer-category-hover');
		},
		function(){
			$(this).parent().find('.offer-category').removeClass('offer-category-hover');
		}
	)

	$('.offer-category').hover(
		function(){
			$(this).parent().find('.offer-name').addClass('offer-name-hover');
		},
		function(){
			$(this).parent().find('.offer-name').removeClass('offer-name-hover');
		}
	)

	$('.menu-item').hover(
		function(){
			$(this).parent().addClass('hover-tab');
		},
		function(){
			$(this).parent().removeClass('hover-tab');
		}
	)

	$('.subject-logo').hover(
		function(){
			$(this).find('.subj-overlay').show();
		},
		function(){
			$(this).find('.subj-overlay').hide();
		}
	)

	var upZofItem = function(element){
		element.addClass('z5');
		var pElement = element.parent();
		if(pElement.has('.notice')){ pElement.find('.notice').addClass('z5');};
		if(pElement.has('.action-name')){ pElement.find('.action-name').addClass('z5');};
		if(pElement.has('.share-label')){ pElement.find('.share-label').addClass('z5');};
	}

	var downZofItem = function(element){
		element.removeClass('z5');
		var pElement = element.parent();
		if(pElement.has('.notice')){ pElement.find('.notice').removeClass('z5')};
		if(pElement.has('.action-name')){ pElement.find('.action-name').removeClass('z5');};
		if(pElement.has('.share-label')){ pElement.find('.share-label').removeClass('z5');};
	}

	$('.action-logo').mouseover(function(){
		$(this).parent().find('.action-dsc-wrapper').show();
		upZofItem($(this));
	})


	$('.action-dsc-wrapper').mouseleave(function(){
		$(this).hide();
		downZofItem($(this).parent().find('.action-logo'));
	})


	
});